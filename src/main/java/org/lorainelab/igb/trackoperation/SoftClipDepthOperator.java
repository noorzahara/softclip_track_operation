/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lorainelab.igb.trackoperation;

import aQute.bnd.annotation.component.Component;
import com.affymetrix.genometry.BioSeq;
import com.affymetrix.genometry.operator.AbstractAnnotationTransformer;
import com.affymetrix.genometry.operator.Operator;
import com.affymetrix.genometry.parsers.FileTypeCategory;
import com.affymetrix.genometry.symmetry.impl.SeqSymmetry;
import java.util.List;
import org.lorainelab.igb.trackoperation.util.SoftClipUtils;

/**
 *
 * @author noorzahara
 */
public class SoftClipDepthOperator extends AbstractAnnotationTransformer implements Operator {

    public SoftClipDepthOperator(FileTypeCategory fileTypeCategory) {
        super(fileTypeCategory);
    }

    @Override   
    public String getName() {
        return fileTypeCategory.toString().toLowerCase() + "_softclip_depth";
    }
    
    @Override
    public String getDisplay() {
        return "Depth Graph (Soft-clip All)";
    }

    @Override
    public SeqSymmetry operate(BioSeq aseq, List<SeqSymmetry> symList) {
        return SoftClipUtils.getSymmetrySoftclipSummary(symList, aseq, false, null);
    }

    @Override
    public FileTypeCategory getOutputCategory() {
        return FileTypeCategory.Graph;
    }
    
}
